package in.connect2tech.parallel.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
   OneTest.class,
   TwoTest.class
})

public class JunitTestOneSuite {   
}  